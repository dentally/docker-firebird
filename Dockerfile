FROM jacobalberty/firebird:2.5.9-ss

ENV RUBY_VERSION 2.6.3

RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    ca-certificates \
    curl \
    openssl \
    && rm -rf /var/lib/apt/lists/*

RUN gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB && \
    curl -L https://get.rvm.io | bash -s stable
RUN /bin/bash -lc "rvm requirements"
RUN /bin/bash -lc "rvm install $RUBY_VERSION && echo $(rvm gemdir)"
RUN /bin/bash -lc "echo \"source /usr/local/rvm/scripts/rvm\" >> ~/.bashrc && source ~/.bashrc"
RUN /bin/bash -lc "gem install csv fb"
